# This file is part of pybib2html, a translator of BibTeX to HTML.
# https://gitlab.com/sosy-lab/software/pybib2html
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from typing import Any, Callable, Dict, NoReturn
from bibtexparser.bibdatabase import BibDatabase

class BibTexParser:
    def __new__(cls, data: Any | None = ..., **args): ...
    bib_database: BibDatabase
    common_strings: Any
    # workaround for https://github.com/python/mypy/issues/708
    # trick mypy into realizing that 'customization' is not a member method.
    customization: Callable[[Dict[str, str]], Dict[str, str]] | NoReturn
    ignore_nonstandard_types: bool
    homogenize_fields: bool
    interpolate_strings: bool
    encoding: str
    add_missing_from_crossref: bool
    alt_dict: Any
    def __init__(
        self,
        data: Any | None = ...,
        customization: Any | None = ...,
        ignore_nonstandard_types: bool = ...,
        homogenize_fields: bool = ...,
        interpolate_strings: bool = ...,
        common_strings: bool = ...,
        add_missing_from_crossref: bool = ...,
    ) -> None: ...
    def parse(self, bibtex_str, partial: bool = ...): ...
    def parse_file(self, file, partial: bool = ...): ...
